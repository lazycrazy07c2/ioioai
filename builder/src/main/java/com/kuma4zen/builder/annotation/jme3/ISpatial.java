package com.kuma4zen.builder.annotation.jme3;

import com.jme3.scene.Spatial;
import com.kuma4zen.annotation.GenerateBuilder;

@GenerateBuilder(value = Spatial.class, code = """
            public B attachToParent(com.jme3.scene.Node parent) {
                parent.attachChild(this.obj);
                return getThis();
            }\
        """)
public interface ISpatial {
}

package com.kuma4zen.builder.annotation.jme3;

import com.jme3.asset.TextureKey;
import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.font.BitmapText;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Geometry;
import com.jme3.system.AppSettings;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.texture.Texture;
import com.kuma4zen.annotation.GenerateBuilder;

@GenerateBuilder(value = AmbientLight.class, genParents = true)
@GenerateBuilder(value = AppSettings.class, parent = "com.kuma4zen.builder.AbstractBuilder")
@GenerateBuilder(AudioNode.class)
@GenerateBuilder(BitmapText.class)
@GenerateBuilder(CameraNode.class)
@GenerateBuilder(DirectionalLight.class)
@GenerateBuilder(Geometry.class)
@GenerateBuilder(Material.class)
@GenerateBuilder(ParticleEmitter.class)
@GenerateBuilder(TerrainQuad.class)
@GenerateBuilder(Texture.class)
@GenerateBuilder(value = TextureKey.class, parent = "com.kuma4zen.builder.AbstractBuilder")
public interface IBuilder {
}

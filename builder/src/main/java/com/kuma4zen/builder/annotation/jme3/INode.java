package com.kuma4zen.builder.annotation.jme3;

import com.jme3.scene.Node;
import com.kuma4zen.annotation.GenerateBuilder;

@GenerateBuilder(value = Node.class, code = """
            public B attachChild(com.jme3.scene.Spatial child) {
                this.obj.attachChild(child);
                return getThis();
            }\
        """)
public interface INode {
}

package com.kuma4zen.builder;

import lombok.RequiredArgsConstructor;

/**
 * @param <T> The entity type this builder builds.
 * @param <B> The final implementation type of this {@link AbstractBuilder}. Necessary to allow builder methods of
 *            non-final implementations to return the builder instance in the correct type.
 */
@RequiredArgsConstructor
public abstract class AbstractBuilder<T, B extends AbstractBuilder<T, B>> {

    protected final T obj;

    /**
     * Returns the {@link T} instance.
     *
     * @return The {@link T} instance
     */
    public T build() {
        return this.obj;
    }

    /**
     * Returns <code>this</code> in the type of this {@link Object}'s final {@link AbstractBuilder} implementation.
     *
     * @return <code>this</code>
     */
    @SuppressWarnings("unchecked")
    public B getThis() {
        return (B) this;
    }
}

package com.kuma4zen.ioioai;

import com.kuma4zen.builder.gen.jme3.AppSettingsBuilder;

public class Main {

    public static void main(String[] args) {
        var settings = AppSettingsBuilder.create(true)
                .setTitle("IoioaiGame")
                .setFrameRate(30)
                .setFullscreen(true);
        var app = new IoioaiClient();
        app.setSettings(settings.build());
        app.start();
    }
}

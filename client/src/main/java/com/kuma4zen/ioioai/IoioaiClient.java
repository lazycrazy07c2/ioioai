package com.kuma4zen.ioioai;

import com.jme3.app.SimpleApplication;
import com.jme3.system.JmeSystem;

import java.util.logging.Logger;

public class IoioaiClient extends SimpleApplication {

    public static final Logger logger = Logger.getLogger(IoioaiClient.class.getName());

    private void initAssetManager() {
        this.assetManager = new J3OAssetManager(JmeSystem.getPlatformAssetConfigURL());
    }

    @Override
    public void initialize() {
        initAssetManager();
        super.initialize();
    }

    @Override
    public void simpleInitApp() {
    }
}

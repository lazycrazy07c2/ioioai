package com.kuma4zen.ioioai;

import com.jme3.asset.AssetNotFoundException;
import com.jme3.asset.DesktopAssetManager;
import com.jme3.asset.ModelKey;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.export.Savable;
import com.jme3.export.binary.BinaryExporter;
import com.jme3.export.binary.BinaryImporter;
import com.jme3.scene.Spatial;
import org.apache.commons.io.output.TeeOutputStream;

import java.io.*;
import java.net.URL;

public class J3OAssetManager extends DesktopAssetManager {

    public static final String J3O = ".j3o";
    public static final String J3O_CACHE = "resources/j3oCache/";

    public J3OAssetManager(URL configFile) {
        super(configFile);

        var cacheDirectory = new File(J3O_CACHE);
        if (!cacheDirectory.exists()) {
            cacheDirectory.mkdirs();
        }
        this.registerLocator("resources/assets", FileLocator.class);
        this.registerLocator(J3O_CACHE, FileLocator.class);
    }

    @Override
    public Spatial loadModel(ModelKey key) {
        if (key == null
                || key.getName().endsWith(J3O)) {
            return super.loadAsset(key);
        }
        try {
            return super.loadAsset(new ModelKey(key.getName() + J3O));
        } catch (AssetNotFoundException ignore) {
            return saveAndLoad(super.loadAsset(key), key.getName());
        }
    }

    private <T extends Savable> T saveAndLoad(T object, String name) {
        var f = new File(J3O_CACHE + name + J3O);
        var parentDirectory = f.getParentFile();
        if (parentDirectory != null && !parentDirectory.exists()) {
            parentDirectory.mkdirs();
        }
        try {
            var byteArrayOS = new ByteArrayOutputStream();
            var fileOS = new FileOutputStream(f);
            var buffOS = new BufferedOutputStream(fileOS);
            var teeOS = new TeeOutputStream(byteArrayOS, buffOS);
            try (byteArrayOS; fileOS; buffOS; teeOS) {
                var exporter = new BinaryExporter();
                exporter.save(object, teeOS);
                var importer = new BinaryImporter();
                importer.setAssetManager(this);
                return (T) importer.load(byteArrayOS.toByteArray());
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        } catch (FileNotFoundException e) {
            throw new AssertionError(e);
        }
    }
}

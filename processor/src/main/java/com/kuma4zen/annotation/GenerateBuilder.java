package com.kuma4zen.annotation;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@Repeatable(GenerateBuilders.class)
public @interface GenerateBuilder {
    Class<?> value();
    String parent() default "";
    boolean genParents() default false;
    String code() default "";
}

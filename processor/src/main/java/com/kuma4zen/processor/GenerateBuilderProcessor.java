package com.kuma4zen.processor;

import com.kuma4zen.annotation.GenerateBuilder;
import com.kuma4zen.annotation.GenerateBuilders;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.MirroredTypeException;
import javax.tools.Diagnostic;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.Collectors;

@SupportedAnnotationTypes({"com.kuma4zen.annotation.GenerateBuilder", "com.kuma4zen.annotation.GenerateBuilders"})
@SupportedSourceVersion(SourceVersion.RELEASE_22)
public class GenerateBuilderProcessor extends AbstractProcessor {

    private static final String BUILDER = "Builder";
    private static final String TEMPLATE = "builder.vm";
    private static final String ABSTRACT_BUILDER = "com.kuma4zen.builder.AbstractBuilder";

    public record Constructor(String paramTypes, String params) {
    }

    public record Method(String name, String paramTypes, String params) {
    }

    private String packageName = "";
    private final List<String> imports = new ArrayList<>();
    private String implName = "";
    private String implTypeName = "";
    private String implParentName = "";
    private boolean implTypeIsAbstract = false;
    private final List<Constructor> constructors = new ArrayList<>();
    private final List<Method> methods = new ArrayList<>();
    private String implCode = "";

    private String qualifiedName = "";
    private boolean genParents = false;

    public GenerateBuilderProcessor() {
        super();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (annotations.isEmpty()) {
            return true;
        }

        roundEnv.getElementsAnnotatedWithAny(Set.of(GenerateBuilder.class, GenerateBuilders.class)).stream()
                .filter(element -> element.getKind() == ElementKind.INTERFACE)
                .forEach(this::processInterfaceElement);

        return true;
    }

    private void prepareContext(VelocityContext vc) {
        vc.put("packageName", packageName);
        vc.put("imports", imports);
        vc.put("implName", implName);
        vc.put("implTypeName", implTypeName);
        vc.put("implParentName", implParentName);
        vc.put("implTypeIsAbstract", implTypeIsAbstract);
        vc.put("constructors", constructors);
        vc.put("methods", methods);
        vc.put("implCode", implCode);
    }

    private void cleanData() {
        packageName = "";
        imports.clear();
        implName = "";
        implTypeName = "";
        implParentName = "";
        implTypeIsAbstract = false;
        constructors.clear();
        methods.clear();
        implCode = "";

        qualifiedName = "";
        genParents = false;
    }

    private void processInterfaceElement(Element interfaceElement) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE,
                "annotated interface: " + interfaceElement.getSimpleName().toString(), interfaceElement);

        var packageName = ((PackageElement) interfaceElement.getEnclosingElement())
                .getQualifiedName().toString().replace(".annotation", ".gen");

        Arrays.stream(interfaceElement.getAnnotationsByType(GenerateBuilder.class))
                .forEach(annotation -> {
                    this.packageName = packageName;
                    processClass(processAnnotationValues(annotation));
                    cleanData();
                });
    }

    private Class<?> processAnnotationValues(GenerateBuilder annotation) {
        implParentName = annotation.parent();
        genParents = annotation.genParents();
        implCode = annotation.code();
        try {
            return annotation.value();
        } catch (MirroredTypeException mte) {
            try {
                return Class.forName(mte.getTypeMirror().toString());
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void processClass(Class<?> typeClass) {
        implName = typeClass.getSimpleName() + BUILDER;
        qualifiedName = packageName + "." + implName;
        imports.add(typeClass.getCanonicalName());
        implTypeName = typeClass.getSimpleName();
        Class<?> parentClass = null;
        if (implParentName.isEmpty()) {
            var superClass = typeClass.getSuperclass();
            if (superClass.equals(Object.class)) {
                implParentName = ABSTRACT_BUILDER;
            } else {
                implParentName = superClass.getSimpleName() + BUILDER;
                if (genParents) {
                    parentClass = superClass;
                }
            }
        }
        implTypeIsAbstract = Modifier.isAbstract(typeClass.getModifiers());
        setConstructors(typeClass);
        setMethods(typeClass);

        generateBuilderClass();

        if (parentClass != null) {
            processParentClass(parentClass);
        }
    }

    private void processParentClass(Class<?> parentClass) {
        implName = implParentName;
        qualifiedName = packageName + "." + implName;

        imports.clear();
        implParentName = "";
        constructors.clear();
        methods.clear();
        implCode = "";

        processClass(parentClass);
    }

    private void setConstructors(Class<?> typeClass) {
        constructors.addAll(Arrays.stream(typeClass.getConstructors())
                .filter(constructor -> !constructor.isAnnotationPresent(Deprecated.class))
                .map(constructor -> new Constructor(
                        Arrays.stream(constructor.getParameters())
                                .map(parameter -> parameter.getType().getCanonicalName() + " " + parameter.getName())
                                .collect(Collectors.joining(", ")),
                        Arrays.stream(constructor.getParameters())
                                .map(Parameter::getName)
                                .collect(Collectors.joining(", "))))
                .toList());
    }

    private void setMethods(Class<?> typeClass) {
        methods.addAll(Arrays.stream(typeClass.getDeclaredMethods())
                .filter(method -> method.getReturnType().equals(void.class))
                .filter(method -> method.getName().startsWith("add") || method.getName().startsWith("set"))
                .filter(method -> Modifier.isPublic(method.getModifiers()))
                .filter(method -> !method.isAnnotationPresent(Deprecated.class))
                .map(method -> new Method(method.getName(),
                        Arrays.stream(method.getParameters())
                                .map(parameter -> parameter.getType().getCanonicalName() + " " + parameter.getName())
                                .collect(Collectors.joining(", ")),
                        Arrays.stream(method.getParameters())
                                .map(Parameter::getName)
                                .collect(Collectors.joining(", "))))
                .toList());
    }

    private void generateBuilderClass() {
        if (qualifiedName.isEmpty()) {
            return;
        }

        try {
            var vc = new VelocityContext();
            prepareContext(vc);

            var vt = initEngine().getTemplate(TEMPLATE);
            var jfo = processingEnv.getFiler().createSourceFile(qualifiedName);
            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "creating src file: " + jfo.toUri());

            var writer = jfo.openWriter();
            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "applying template: " + vt.getName());

            vt.merge(vc, writer);
            writer.close();
        } catch (Exception e) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getLocalizedMessage());
        }
    }

    private VelocityEngine initEngine() throws Exception {
        var props = new Properties();
        var url = this.getClass().getClassLoader().getResource("velocity.properties");
        assert url != null;
        props.load(url.openStream());
        var ve = new VelocityEngine(props);
        ve.init();
        return ve;
    }
}
